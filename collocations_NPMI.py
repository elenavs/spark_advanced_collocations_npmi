# Извелкаем коллокации из датасета, используя метрику NPMI (нормализованная точечная взаимная информация). Есть список стоп-слов (мусор), его не учитываем.

from pyspark import SparkContext
from pyspark import SparkConf

import math
import re

config = SparkConf().setAppName("task1").setMaster("yarn")
sc = SparkContext(conf=config)

# парсим и отбрасываем стоп слова 

def parsing(line):
    article_id, text = line.strip().split('\t', 1)
    text = re.sub("^\W+|\W+$", "", text.lower())
    words = re.split("\W*\s+\W*", text)
    return words

def stop_words_filter(x):
    x_update = list()
    for w in x:
        if w not in stop_words_fin:
            x_update.append(w)
    return x_update

def bigrams(words):
    bigram = list()
    for i in range(0, len(words) - 1):
        b = words[i]
        b += '_'
        b += words[i+1]
        bigram.append(b)
    return bigram

stop_words = sc.textFile("/data/wiki/stop_words_en-xpo6.txt")
stop_words = stop_words.map(lambda x: x.strip().lower())
stop_words_fin = stop_words.collect()

rdd = sc.textFile("/data/wiki/en_articles_part")
rdd2 = rdd.map(lambda x: parsing(x))
rdd3 = rdd2.map(lambda x: stop_words_filter(x))

words_rdd = rdd.flatMap(lambda x: parsing(x))
cnt_words = words_rdd.count()
cnt_bigram = cnt_words - rdd2.count()

words_rdd2 = words_rdd.filter(lambda x: x not in stop_words_fin)
words_rdd3 = words_rdd2.map(lambda x: (x, 1))
words_rdd4= words_rdd3.reduceByKey(lambda a, b: a + b)
words_rdd5 = words_rdd4.sortBy(lambda a: -a[1])

words_var = words_rdd5.collect()
words = dict()
words.update(words_var)

bigram_rdd = rdd3.flatMap(lambda x: bigrams(x))
bigram_rdd2 = bigram_rdd.map(lambda x: (x, 1))
bigram_rdd3= bigram_rdd2.reduceByKey(lambda a, b: a + b)
bigram_rdd4 = bigram_rdd3.filter(lambda x: x[1] >= 500)
bigram_rdd5 = bigram_rdd4.sortBy(lambda a: -a[1])
bigram = bigram_rdd5.take(38)

NPMI = list()
for word_pair, cnt in bigram:
    word_1, word_2 = word_pair.split("_")
    PMI = math.log((cnt_words ** 2) * cnt/(words[word_1] * words[word_2] * cnt_bigram))
    NPMI_t = (0 - PMI) / math.log((cnt / cnt_bigram) + 0.000001)
    NPMI.append((word_pair, NPMI_t))

for word, count in sorted(NPMI, key=lambda a: -a[1]):
    print("{}".format(word))

